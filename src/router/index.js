import Vue from 'vue'
import Router from 'vue-router'
import MembersList from '@/components/MembersList'
import Edit from '@/components/Edit'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Список участниц',
      component: MembersList
    }, {
      path: '/edit/:id',
      component: Edit
    }
  ]
})
